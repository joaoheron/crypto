from cryptography.fernet import Fernet
FILEPATH = '/home/joao/workspace/crypto/crypt/company.env'
KEYPATH = '/home/joao/workspace/crypto/crypt/xavinha_esperta.key'
file_to_encrypt = ''
file_to_encrypt = FILEPATH

# get the key
file = open(KEYPATH, 'rb')
key = file.read()
file.close()

# encrypt data
with open(FILEPATH, 'rb') as f:
    data = f.read()
fernet = Fernet(key)
encrypted = fernet.encrypt(data)

# write the encrypted file
with open(FILEPATH+'.encrypted', 'wb') as f:
    f.write(encrypted)

