from cryptography.fernet import Fernet
FILEPATH = '/home/joao/workspace/crypto/crypt/company.env.encrypted'
KEYPATH = '/home/joao/workspace/crypto/crypt/xavinha_esperta.key'
file_to_encrypt = ''
file_to_encrypt = FILEPATH

# get the key
file = open(KEYPATH, 'rb')
key = file.read()
file.close()

# decrypt data
with open(FILEPATH, 'rb') as f:
    data = f.read()
fernet = Fernet(key)
decrypted = fernet.decrypt(data)

# write the decrypted file
with open(FILEPATH[:-10]+'.decrypted', 'wb') as f:
    f.write(decrypted)

